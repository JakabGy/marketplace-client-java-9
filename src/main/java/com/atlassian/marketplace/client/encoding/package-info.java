/**
 * Interfaces and classes related to JSON serialization and deserialization.
 * @since 2.0.0
 */
package com.atlassian.marketplace.client.encoding;