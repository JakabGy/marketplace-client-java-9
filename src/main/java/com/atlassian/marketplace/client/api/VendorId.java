package com.atlassian.marketplace.client.api;

import java.net.URI;

/**
 * Simple value wrapper for a resource URI when it is used to identify a vendor.
 * @since 2.0.0
 */
public final class VendorId extends ResourceId
{
    private VendorId(URI uri)
    {
        super(uri);
    }
    
    public static VendorId fromUri(URI uri)
    {
        return new VendorId(uri);
    }
}
